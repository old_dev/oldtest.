package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnClear;
    Button btnClear2;
    Button btnClearAss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnClear = (Button)findViewById(R.id.button);
        btnClear.setOnClickListener(this);

        btnClear2 = (Button)findViewById(R.id.button2);
        btnClear2.setOnClickListener(this);

        btnClearAss = (Button)findViewById(R.id.button3);
        btnClearAss.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                Intent SqlAss = new Intent(".SqlInTheAss");
                startActivity(SqlAss);
                break;
            case R.id.button2:
                Intent SqlAss2 = new Intent(".Sql2InTheAss");
                startActivity(SqlAss2);
                break;
            case R.id.button3:
                Intent Ass = new Intent(".Screen_2");
                startActivity(Ass);
                break;
        }
    }

}
